﻿using System;
using System.Collections.Generic;
using System.Text;
using Dziedziczenie;

namespace ag054404
{
    class Corsa : IPojazd
    {
        private SilnikElektryczny silnik;

        public Corsa(SilnikElektryczny silnik)
        {
            this.silnik = silnik;
        }
        public void Jedz()
        {
            this.Jedz(50);
        }

        public void Jedz(int dystans)
        {
            for (int i = 1; i <= dystans; i++)
            {
                if (i > silnik.MaksymalnyDystans)
                {
                    Console.WriteLine("Rozładowany akumulator ;C");
                    break;
                }
                Console.WriteLine(i + "km..." + silnik.Dzwiek);
            }
        }
    }
}
