﻿using System;
using Dziedziczenie;

namespace FC49615
{
    class Program
    {
        static void Main(string[] args)
        {
            var myEngine = new SilnikElektryczny(500,3);
            var myGasolineEngine = new SilnikBenzynowy(280,12);
            var myCar = new Kamaz(myEngine, myGasolineEngine);
            myCar.Jedz(20);
            //Console.WriteLine("Hello World!");
        }
    }
}