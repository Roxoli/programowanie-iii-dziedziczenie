﻿using System;
using Dziedziczenie;

namespace PK043585
{
    class Program
    {
        static void Main(string[] args)
        {
            V8 silnik = new V8(550, 50);
            var samochodzik = new BMW(silnik);
            samochodzik.Jedz(350);
        }
    }
}
